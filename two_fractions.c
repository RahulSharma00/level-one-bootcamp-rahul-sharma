#include<stdio.h>

struct fraction{
  int num;
  int denom;
};


int input(){
  int a;
  scanf("%d",&a);
  return a;
}

int gcd_find(int a,int b){
  if(b == 0)
        return a;
    else
        return gcd_find(b, a%b);
}      
struct fraction add(struct fraction f1,struct fraction f2)
{
  struct fraction sum;
  sum.num = (f1.num*f2.denom+f1.denom*f2.num);
  sum.denom = f1.denom*f2.denom;
  int gcd = gcd_find(sum.num,sum.denom);
  sum.num = sum.num/gcd;
  sum.denom = sum.denom/gcd;
  return(sum);
}
void print(struct fraction f1,struct fraction f2, struct fraction sum)
{
  printf("%d/%d + %d/%d = %d/%d",f1.num,f1.denom,f2.num,f2.denom,sum.num,sum.denom);
}


int main()
{
  struct fraction  f1,f2,sum;
  printf("enter the num1 and denom1:");
  f1.num = input();
  f1.denom = input();
  printf("enter the num2 and denom2: ");
  f2.num = input();
  f2.denom = input();
  sum = add(f1,f2);
  print(f1,f2,sum);
  return 0;
}


//WAP to find the sum of two fractions.